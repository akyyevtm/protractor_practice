exports.config = {
    framework : 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    capabilities:{ browserName: 'chrome'},
    getPageTimeout: 120000,
    waitForAngualarEnabled: true,
    // restartBrowserBetweenTests: true,

    specs: ['../specs/*.js'],
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    },
    onPrepare: function(){ 
        browser.waitForAngularEnabled(false);
        browser.manage().timeouts().implicitlyWait(10000);
        browser.manage().window().maximize();

        let HTMLReporter = require("protractor-beautiful-reporter");
        jasmine
            .getEnv()
            .addReporter( 
                new HTMLReporter(  
                    {
                        baseDirectory: "Reports/Holland_America_Reports",
                        takeScreenShotsOnlyForFailedSpecs: true
                    }
                )
                .getJasmine2Reporter()
            );
    }
}

// You can do this as well to export but make sure you have config object with value
// module.exports = {config}

console.log(module);