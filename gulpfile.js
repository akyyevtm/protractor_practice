var gulp = require('gulp');
var protractor = require("gulp-protractor").protractor;

function defaultTask(cb) {
    gulp.src(['./specs/*.js'])
            .pipe(protractor({
                configFile: "./config/protractor.conf.js",
                debug: false,
                autoStartStopServer: true
            }))
            .on('error', function(err) {
                console.log(err);
                throw err;
            })
    cb();
}
  
//   exports.default = defaultTask

gulp.task('default', defaultTask)



gulp.task('someTask', (cb)=>{
    gulp.src(['./specs/*.js'])
            .pipe(protractor({
                configFile: "./config/protractor.conf.js",
                args: ['--baseUrl', 'http://127.0.0.1:8000'],
                debug: false,
                autoStartStopServer: true,
                webDriverUpdate: {
                    browsers: ['safari']
                }
            }))
            .on('error', function(err) {
                console.log(err);
                throw err;
            })
            .on('end', cb);
    cb();
})

