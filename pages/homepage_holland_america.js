const { browser, element, by } = require("protractor");
var EC = require('protractor').protractor.ExpectedConditions;

var elements = function(){
    // this.login_register = element(by.className('nav-link login caret'));
    this.login_register = $('.nav-link.login.caret');
    this.account = element(by.xpath("//a[@class='btn-login']/span"));
    this.plan = element(by.xpath("//ul[@class='main-nav']/li//a[contains(text(), 'Plan')]"));
    this.languageDropdown = element(by.className("dropdown-label"));
    this.eng = $("#utility-nav-dropdown-0");
    this.deu = $("#utility-nav-dropdown-1");
    this.spa = element(by.id("utility-nav-dropdown-2"));
    this.dut = $("#utility-nav-dropdown-3");

    this.popupCloseButton = element(by.css('.close.me483close'));

    this.navigate = (module, submodule)=>{
        var mod = element(by.xpath("//a[@class='main-nav-link ' and text()='"+module+"']"));
        var sub_mod = element(by.css("a[data-linktext='primary Nav:"+module+":"+submodule+"']"));

        browser.wait(EC.elementToBeClickable(mod),5000);
        mod.click();
        
        browser.wait(EC.elementToBeClickable(sub_mod),5000);
        browser.wait(EC.presenceOf(sub_mod),5000);
    
        sub_mod.click();

    }

    
};

module.exports = new elements();