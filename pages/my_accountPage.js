const { element, by } = require("protractor")

var elements = function(){
    this.loginEmail = element(by.id('-login-email'));
    this.loginPass = element(by.id('-login-password'));

    this.loginButton = element(by.className('allow-redirect'));
    this.errorMessage = element(by.css('.error-summary-wrapper>p'));
}

// module.exports = new elements();

module.exports = {
    pageElements: new elements()
}