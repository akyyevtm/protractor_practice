exports.config = {
    directConnect: true,
    framework : 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
      'browserName': 'chrome',
	  'shardTestFiles': true,
      'maxInstances': 2
    },
    specs: [
        'input_spec.js',
        'mat_paginator_spec.js'
    ],

    jasmineNodeOpts: {
      defaultTimeoutInterval: 30000
    }
};