const { browser, element, by } = require("protractor");
var testData = require('../Resources/holland_america.json');
var holland_america_homepage = require('../pages/homepage_holland_america.js');
var EC = require('protractor').protractor.ExpectedConditions;
var {pageElements: accountPage} = require('../pages/my_accountPage.js');


describe('Holland America Tests', () => {
  var title;
  
  beforeEach(function(){
    console.log('----before test case----');
    browser.manage().deleteAllCookies();
    browser.get(testData.URL);
    //just to verify is loaded to interact with
    browser.wait(EC.elementToBeClickable(holland_america_homepage.languageDropdown), 5000);
  });

  it('1. Homepage has the expected page title', () => {
    expect(browser.getTitle()).toEqual(testData.title.homepage);
  });

  it('2. Verify page title when selecting Deutsch as a user language', () => {
    holland_america_homepage.languageDropdown.click();
    holland_america_homepage.deu.click();
    expect(browser.getTitle()).toEqual(testData.title.deu);
  });


  it('3. Verify page title for `Find a Cruise` module and verify it contains "RESULTS"', () => {
    holland_america_homepage.navigate('Plan', 'Find a Cruise');
    browser.wait(EC.elementToBeClickable(holland_america_homepage.languageDropdown));
    expect(browser.getTitle()).toEqual(testData.title.find_a_cruies);
    
    var result = element(by.className('results'));
    expect(result.getText()).toContain('RESULTS');

    result.getText().then(elementText => {
      console.log(elementText);
    })

  });

  it('4. Verify page title for `Where We Go` module', () => {
    holland_america_homepage.navigate('Where We Go', 'Alaska');
    expect(browser.getTitle()).toEqual(testData.title.where_we_go);
  });

  it('5. Goto my `Account` page and verify error message', () => {
    holland_america_homepage.account.click();
    
    browser.wait(EC.elementToBeClickable(holland_america_homepage.languageDropdown));
    browser.wait(EC.presenceOf(accountPage.loginEmail));

    accountPage.loginEmail.sendKeys(testData.FAKE_EMAIL);
    accountPage.loginPass.sendKeys(testData.FAKE_PASSWORD);
    accountPage.loginButton.click();
    expect(accountPage.errorMessage.getText()).toEqual('Please enter a valid email address and password.');
  });

  it('6. Failing test on purpose, now it should pass', () => {
    holland_america_homepage.navigate('Plan', 'Holiday Cruises');
    // expect(browser.getTitle()).toEqual('Incorrect Title');
    expect(browser.getTitle()).toEqual(testData.title.holiday_cruises);
  });

});