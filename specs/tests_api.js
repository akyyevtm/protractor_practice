const { browser, element, by } = require("protractor");
var Request = require("request");

describe("Errors in Protractor",function(){
  browser.ignoreSynchronization = true; // for non-angular websites
  it("Error handleing in protractor",(cb)=>{

    Request.get({
      "headers": { "content-type": "application/json" },
      "url": "https://chercher.tech/sample/api/product/read?id=90"

      }, (error, response, body) => {
        if(error) {
            return console.log(error);
        }
        console.log("Body : ******");
        console.log(JSON.parse(body));

        console.log("\nResponse Code ****:"+response.statusCode)
        expect(response.statusCode).toBe(200)
        cb();
    });
  });
});


describe("Errors in Protractor",function(){
  browser.ignoreSynchronization = true; // for non-angular websites
  it("api Testing in protractor",function(done){

    Request.put({
      "headers": { "content-type": "application/json" },
      "url": "https://chercher.tech/sample/api/product/create",
      "body": JSON.stringify({
        "name": "some stupid guy",
        "description": "90033"
      })

      }, (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        console.dir("Body : ******");
        console.dir(response.body);

        console.log("\n\nHeader ****:")
        console.log(response.headers)

        expect(response.body).toContain('Product was created')
        expect(response.headers.connection).toBe('close')
        expect(response.headers.server).toBe('cloudflare')
        // this below line took half day of research
        done();
    });
  });
});