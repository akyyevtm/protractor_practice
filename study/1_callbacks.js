// callback functions - higher order functions
// functions are actually first class objects, we can pass functions to another function as an argument
let x = function(){
    console.log('I am called from inside a function');
}

// direct call
x();

// Example of callback function...
let y = function(callback){
    console.log('do something');
    callback();
}

y(x);


let add = function(a,b){
    return a+b;
}
let multiply = (a,b)=> a*b;

function subtract(a,b){
    return a-b;
}

let doWhatever = function(a,b){
    console.log(`Here are your two numbers back ${a} and ${b}`);
}
//Another example
//here calcType is a function passed as an argument
let calc = function(num1, num2, calcType){
    if(typeof calcType === 'function') 
        return calcType(num1,num2);
}

calc(2,3,doWhatever);
console.log( calc(2,3,add));
console.log(calc(2,3,multiply));
console.log(calc(2,3,subtract));



// JS has own sort function but we can modify it by passing a callback function
// Example 
let myArr = [
    {
        num: 5,
        str: 'apple'
    },
    {
        num: 7,
        str: 'cabbage'
    },
    {
        num:1,
        str: 'banana',
        color: 'green'
    }
]

// myArr.sort((a,b)=>{
//     if(a.num>b.num) return 1;
//     else if((a.num==b.num)) return 0;
//     else return -1;
// });

//or
let sortWay = (a,b)=>{
        if(a.num>b.num) return 1;
        else if((a.num==b.num)) return 0;
        else return -1;
    }

myArr.sort(sortWay);
console.log(myArr);




const posts = [
    {title: 'Post 1', body: 'This is post one'},
    {title: 'Post 2', body: 'This is post two'}
]

function getPosts(){
    setTimeout(()=>{
        let output = '';
        posts.forEach((post, index)=>{
            output += `<li>${post.title}</li>`
        })
        document.body.innerHTML = output;
    }, 1000);
}

function createPost(post, callback){
    setTimeout(()=>{
        posts.push(post);
        callback();
    },2000);
}

// it will add post 3 to the posts then it will call getPosts function
createPost({title: 'Post 3', body: 'This is post three'}, getPosts)
