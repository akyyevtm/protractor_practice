
const posts = [
    {title: 'Post 1', body: 'This is post one'},
    {title: 'Post 2', body: 'This is post two'}
]

function getPosts(){
    setTimeout(()=>{
        let output = '';
        posts.forEach((post, index)=>{
            output += `<li>${post.title}</li>`
        })
        document.body.innerHTML = output;
    }, 1000);
}

function createPost(post){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            posts.push(post);

            const error = false;
            // const error = true;

            if(!error) resolve();
            else reject('Error: something went wrong!')
        },2000);
    })
}

//now createPost returns Promise
// so we can use .then syntax and pass getPosts function
// 
// createPost({title: 'Post 3', body: 'This is post three'})
//     .then(getPosts)
//     .catch((err)=>console.log(err));


// Promise.all
const promise1 = Promise.resolve('Hello World');
const promise2 = 10;
const promise3 = new Promise((resolve, rej)=>{
    setTimeout(resolve, 2000, 'Good bye!');

})
const promise4 = fetch('https://jsonplaceholder.typicode.com/users').then(res=>res.json())

Promise.all([promise1,promise2,promise3,promise4])
    .then(
            (values)=>console.log(values)
        );
