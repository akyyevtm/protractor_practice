
const baseURI = 'https://jsonplaceholder.typicode.com'

fetch(baseURI+'/users')
    .then(res=>{
        console.log(res.status, res.statusText);
        return res;
    })
    .then(response => response.json())
    .then(json => {
        console.log(json);
        console.log(json[0].name);
        console.log(json[3].address.geo);
        console.log(json[0].username==='Bret')
        return json;
    }).then(json=>{
        json.forEach((element, index )=> {
            console.log('Element', index, element.name)
        });
    })
    .catch(err=>console.log(err))


fetch(baseURI+'/posts',{
    method: 'POST',
    body: JSON.stringify({
        title: 'foo',
        body:'bar',
        userId: 1
    }),
    headers: {
        "Content-type": "application/json"
    }
})
    .then(res=>res.json())
    .then(json=>{
        console.log(json)
        return json
    }).then(json=>{
        
    })
