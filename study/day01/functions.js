

// Examples:


let exampleFunction = function(){
    return 'I am a function'
}

exampleFunction = ()=>{
    return 'I am a function'
}

exampleFunction = () => 'I am a function';

//return an object
exampleFunction = ()=>{
    return {
        'power level': 9001,
        'isHungary': true
    }
}

//same as above
exampleFunction = ()=> ({
    'power level': 9001,
    'isHungary': true
})

console.log(exampleFunction())
console.log(0 || 'A');
console.log('A' || 0);
