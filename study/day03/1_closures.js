
// const add = (a,b) => a+b

// const calculate = (func, arg) => func(...arg)

// const add10 = (numb)=> calculate(add, [numb, 10])

// console.log(add10(20))



// var topla = (function(){
//     var counter = 0
//     return function(){
//         counter+=1
//         console.log(counter);
//         return counter
//     }
// })()

// topla()
// topla()
// console.log(topla())

//Closure means that an inner function always has access to the vars and 
//parameters of its outer function, even after the outer function has returned.
function OuterFunction() {
    var outerVariable = 1;

    function InnerFunction() {
        console.log(outerVariable);
    }

    InnerFunction();
}

OuterFunction()


//In the above example, return InnerFunction; returns InnerFunction from OuterFunction when you call OuterFunction(). 
//A variable innerFunc reference the InnerFunction() only, not the OuterFunction(). 
//So now, when you call innerFunc(), it can still access outerVariable which is declared in OuterFunction(). 
//This is called Closure.
function OuterFunction2() {

    var outerVariable = 100;

    function InnerFunction() {
        console.log(outerVariable);
    }

    return InnerFunction;
}
var innerFunc = OuterFunction2();

innerFunc(); 


function increment(){
    var num = 0

    function inc(){
        num+=1
        return num
    }

    return inc
}

let increasing = increment() // this is a function

console.log(increasing()); // Here we're calling that function
console.log(increasing());
console.log(increasing());


console.log('---------------------------');


var counter = function(){
    var privateCounter = 0

    function changeValue(val){
        privateCounter += val
    }

    return {
        increment: function(){
            changeValue(+1)
        },
        decrement: function(){
            changeValue(-1)
        },
        value: function(){
            return privateCounter
        }
    }
}
//In the above example, increment(), decrement() and value() becomes public function because they are included in the return object, 
//whereas changeValue() function becomes private function because it is not returned and only used internally by increment() and decrement().
var myCounter = counter()

myCounter.increment()
myCounter.increment()
myCounter.decrement()
myCounter.increment()
myCounter.increment()

console.log(myCounter.value());



(function method(){
    var a=b=5
})()

console.log(b); // 5