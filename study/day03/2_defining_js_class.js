
// First way of creating JS class
class Person{
   constructor(name, lastName, age=30) {
       this.name = name
       this.lastName = lastName
       this.age = age
   } 

   displayPerson = ()=>{
       return `My name is ${this.name} ${this.lastName} and I am ${this.age} years old`
   }
}

const p1 = new Person('Mike', 'Tyson', 45)
console.log(p1.displayPerson());


// Second way using functions

function Course(name, duration, difficulty='beginner'){
    Person.counter++

    this._name = name || 'Unnamed'
    this._duration = duration || 100
    this._difficulty = difficulty

    Object.defineProperties(this, {
        name:{
            get: ()=> this._name, // this is getter
            set: (newName) => this._name = newName
        },
        duration:{
            set: (val)=> this._duration = val
        },
        level:{
            get: ()=> this._difficulty
        }
    })
}
Person.counter = 0

const c1 = new Course('JS', 5, 'intermediate')
const c2 = new Course('Python')


c1.level = 'Beginner' // This will not effect attribute of the object, cuz there is no setter
c1.name = 'Java'
c1.duration = 20

console.log(c1.name); // JS-> Java
console.log(c1.level); // Still intermediate
console.log(c1.duration); // undefined
console.log(c1._duration); // 20

console.log('Number of created objects: ', Person.counter);
console.log(c1);

