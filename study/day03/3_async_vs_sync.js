
const greet = (name)=> console.log(`Hello there...${name}`);


setTimeout(greet, 2000, 'Bob') // Asyncronous method will be executed at the end
setTimeout(greet, 2000, 'Mike')

for(let i=0; i<4; i++){
    setTimeout(function(val) {alert(val)}, 500, i+' For Loop')
}
greet('John') // Syncronous

console.log('----------last statement---------');