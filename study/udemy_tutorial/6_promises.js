
let myPromise = new Promise(function(resolve, reject){

    let isComplete = true

    if(isComplete){
        resolve('This is complete!')
    } else {
        reject('This is NOT complete!')
    }
})

// asyncronous
myPromise
    .then((result)=>{
        console.log(result);
    })
    .catch((err)=>{
        console.log(`err happened: `+err);
    })

// syncronous
console.log('My name is Bob');


//chaining promises 
// Anonymous function
let turnOnComputer = function(){
    return new Promise((resolve, rej)=>{
        resolve('Computer is ON!')
    })
}

let openBrowser = function(msg){
    return new Promise((resolve, rej)=>{
        resolve('Browser has been opened!')
    })
}

let goToPage = function(msg){
    return new Promise((resolve, rej)=>{
        if(50>6){
            resolve('Navigating to the PAGE!')
        } else {
            rej('Error happened!')
        }
    })
}

turnOnComputer()
    .then((result)=>{
        console.log(result);
        return openBrowser(result)
    })
    .then((result)=>{
        console.log(result);
        return goToPage(result);
    })
    .then((result)=>{
        console.log(result);
        console.log('We are DONE!');
    }).catch(err=> console.log(err))

Promise.all([turnOnComputer(), openBrowser(), goToPage()])
    .then((res)=>{
        console.log('DONE');
    })